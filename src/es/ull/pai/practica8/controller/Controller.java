/**
 * 
 */
package es.ull.pai.practica8.controller;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import es.ull.pai.practica8.model.Bound;
import es.ull.pai.practica8.model.Circle;
import es.ull.pai.practica8.view.BallWindow;
import es.ull.pai.practica8.view.Canvas;
import es.ull.pai.practica8.view.Directions;

/**
 * @author eleazardd
 *
 */
public class Controller {
	private Bound constraints;
	private Canvas canvas;
	private BallWindow ballWindow;
	private Directions directions;
	private Circle shape; 
	
	public Controller(int steps, int radius) {
		directions = new Directions();
		canvas = new Canvas();
		ballWindow = new BallWindow(directions, canvas);
		ballWindow.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				updateBound();
				shape = new Circle(canvas.getWidth() / 2, canvas.getHeight() / 2, radius);
				shape.recalculate(constraints);
				updateCanvas();
				
			}
		});
		
		ballWindow.setVisible(true);
	
		shape = new Circle(canvas.getWidth() / 2, canvas.getHeight() / 2, radius);

		constraints = new Bound(0, 0, canvas.getWidth() , canvas.getHeight());
		
		directions.getLeftBtn().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shape.left(steps, constraints);
				updateCanvas();
			}
		});
		
		directions.getRightBtn().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shape.right(steps, constraints);
				updateCanvas();
			}
		});

		directions.getUpBtn().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shape.up(steps, constraints);
				updateCanvas();
			}
		});
		
		directions.getDownBtn().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shape.down(steps, constraints);
				updateCanvas();
			}
		});
		
	}
	
	private void updateBound() {
		System.out.println("daff");
		constraints = new Bound(0, 0, canvas.getWidth() , canvas.getHeight());
	}
	
	private void updateCanvas() {
		canvas.setPosX(shape.getPosX());
		canvas.setPosY(shape.getPosY());
		canvas.setRadius(shape.getRadius());
		canvas.repaint();
	}
	
	/**
	 * @return the constraints
	 */
	public Bound getConstraints() {
		return constraints;
	}

	/**
	 * @return the canvas
	 */
	public Canvas getCanvas() {
		return canvas;
	}

	/**
	 * @return the ballWindow
	 */
	public BallWindow getBallWindow() {
		return ballWindow;
	}

	/**
	 * @return the directions
	 */
	public Directions getDirections() {
		return directions;
	}

	/**
	 * @return the shape
	 */
	public Circle getShape() {
		return shape;
	}
}
