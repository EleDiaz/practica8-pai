/**
 * 
 */
package es.ull.pai.practica8.model;

/**
 * @author Eleazar Díaz Delgado
 * Its assume that coordinates start at upperleft corner of screen like swing
 * does.
 * 
 */
public class Bound {
	private int posX;
	private int posY;
	private int height;
	private int width;

	public Bound(int x, int y, int w, int h) {
		posX = x;
		posY = y;
		width = w;
		height = h;
	}

	public int leftLimit() {
		return posX;
	}

	public int rightLimit() {
		return posX + width;
	}

	public int upLimit() {
		return posY;
	}

	public int downLimit() {
		return height + posY;
	}
}
