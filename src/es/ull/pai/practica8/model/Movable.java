/**
 * interface Movable 
 * 
 * Version: 0.1.0
 *
 * Created dom abr 10 13:33:08 WEST 2016
 *
 * Copyright Eleazar Díaz Delgado
 */
package es.ull.pai.practica8.model;

/**
 * @author Eleazar Díaz Delgado
 *
 * Represents capacity of an object to move over its surface with a determinate Bound.
 * @see es.ull.pai.practica8.model.Bound
 */
public interface Movable {

	public void left(int steps, Bound constraints);
	
	public void right(int steps, Bound constraints);
	
	public void up(int steps, Bound constraints);
	
	public void down(int steps, Bound constraints);
}
