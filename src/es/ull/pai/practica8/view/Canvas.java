/**
 * 
 */
package es.ull.pai.practica8.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * @author eleazardd
 *
 */
public class Canvas extends JPanel {
	private final int MIN_SIZE = 200; 		// Preferred size in pixels
	private int posX;
	private int posY;
	private int radius;
	

	public Canvas() {
		setPreferredSize(new Dimension(MIN_SIZE, MIN_SIZE));
		setBackground(Color.CYAN);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		System.out.println(" " +posX +"   "+ posY);
		new Circle(g, posX - radius, posY - radius, radius * 2, radius * 2);
	}

	/**
	 * @return the posX
	 */
	public int getPosX() {
		return posX;
	}

	/**
	 * @param posX the posX to set
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}

	/**
	 * @return the posY
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * @param posY the posY to set
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}

	/**
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}
}
