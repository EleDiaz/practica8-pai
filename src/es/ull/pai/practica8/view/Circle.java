/**
 * 
 */
package es.ull.pai.practica8.view;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author eleazardd
 *
 */
public class Circle {
	private Color color = Color.RED;
	
	public Circle(Graphics g, int x, int y, int width, int height) {
		Color aux = g.getColor();
		g.setColor(color);
		g.fillOval(x, y, width, height);
		g.setColor(aux);
	}

}
