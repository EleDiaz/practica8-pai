/**
 * 
 */
package es.ull.pai.practica8.view;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JButton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * @author eleazardd
 *
 */
public class Directions extends JPanel {
	private final String UP_TXT = "Up";
	private final String DOWN_TXT = "Down";
	private final String LEFT_TXT = "Left";
	private final String RIGHT_TXT = "Right";
	private JButton leftBtn;
	private JButton rightBtn;
	private JButton downBtn;
	private JButton upBtn;
	

	public Directions() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		setLayout(gridBagLayout);
		
		upBtn = new JButton(UP_TXT);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.gridwidth = 2;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 0;
		add(upBtn, gbc_btnNewButton);
		
		leftBtn = new JButton(LEFT_TXT);
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.gridwidth = 2;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 0;
		gbc_btnNewButton_1.gridy = 1;
		add(leftBtn, gbc_btnNewButton_1);
		
		rightBtn = new JButton(RIGHT_TXT);
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.gridwidth = 2;
		gbc_btnNewButton_2.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton_2.gridx = 2;
		gbc_btnNewButton_2.gridy = 1;
		add(rightBtn, gbc_btnNewButton_2);
		
		downBtn = new JButton(DOWN_TXT);
		GridBagConstraints gbc_btnNewButton_3 = new GridBagConstraints();
		gbc_btnNewButton_3.gridwidth = 2;
		gbc_btnNewButton_3.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton_3.gridx = 1;
		gbc_btnNewButton_3.gridy = 2;
		add(downBtn, gbc_btnNewButton_3);
		
	}
	
	/**
	 * @return the leftBtn
	 */
	public JButton getLeftBtn() {
		return leftBtn;
	}

	/**
	 * @return the rightBtn
	 */
	public JButton getRightBtn() {
		return rightBtn;
	}

	/**
	 * @return the downBtn
	 */
	public JButton getDownBtn() {
		return downBtn;
	}

	/**
	 * @return the upBtn
	 */
	public JButton getUpBtn() {
		return upBtn;
	}
}
